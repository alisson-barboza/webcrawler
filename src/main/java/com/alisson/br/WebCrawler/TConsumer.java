package com.alisson.br.WebCrawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

public class TConsumer extends Thread {

	private Buffer buffer;

	public TConsumer(Buffer instance) {
		this.buffer = instance;
	}

	@Override
	public void run() {
		String imageUrl = buffer.getImgLink();
		if (imageUrl != null) {

			try {
				Connection.Response resultImageResponse = Jsoup.connect(imageUrl).ignoreContentType(true).execute();
				File file = File.createTempFile("image", ".png");
				file = new File(file.getName());
				try (FileOutputStream out = (new FileOutputStream(file))) {
					out.write(resultImageResponse.bodyAsBytes());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			return;
		}
	}
}
