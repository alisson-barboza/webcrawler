package com.alisson.br.WebCrawler;

import java.util.List;

public class TPreProducer extends Thread {

	private List<String> urls;
	private Buffer buffer;
	public TPreProducer(List<String> urls, Buffer buffer) {
		this.urls = urls;
		this.buffer = buffer;
	}
	
	@Override
	public void run() {
		for(String site: urls) {
    		new TProducer(site, buffer).start();
    	}
	}
	
}
