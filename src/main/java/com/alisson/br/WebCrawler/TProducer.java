package com.alisson.br.WebCrawler;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TProducer extends Thread{

	private String webSiteUrl;
	private Buffer buffer;
	
	public TProducer(String webSiteUrl, Buffer buffer) {
		this.buffer = buffer;
		this.webSiteUrl = webSiteUrl;
	}
	
	@Override
	public void run() {
		try {
			Document document = Jsoup.connect(webSiteUrl).get();
			Elements images = document.select("img[src~=(?i)\\.(png|jpe?g|gif)]");
			for(Element img : images) {
				String src = img.absUrl("src");
				this.buffer.addImgLink(src);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
