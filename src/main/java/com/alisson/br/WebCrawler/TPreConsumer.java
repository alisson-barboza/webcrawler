package com.alisson.br.WebCrawler;

public class TPreConsumer extends Thread{
	
	private static volatile TPreConsumer instance;
	
	private TPreConsumer() {		
	}
    public static synchronized TPreConsumer getInstance(){
        if(instance == null){
            instance = new TPreConsumer();
            
            return instance;
        }
        return instance;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
	
	@Override
	public void run() {
		Buffer buffer = Buffer.getInstance();
		while(true) {
			if(buffer.getSize() == 0) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}else {
				new TConsumer(Buffer.getInstance()).start();
			}
		}
			
	}
	public Thread getThread() {
		return this;
	}
}
