package com.alisson.br.WebCrawler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Buffer {

	private static Buffer instance = null;
	private List<String> imgLinks = Collections.synchronizedList(new ArrayList());
		
	private Buffer() {
	}
    public static synchronized Buffer getInstance(){
        if(instance == null){
            instance = new Buffer();
            return instance;
        }
        return instance;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException(); 
    }

	public String getImgLink() {
		if(imgLinks.size() != 0) {
			return imgLinks.remove(0);
		}
		return null;
	}
	
	public Integer getSize() {
		return this.imgLinks.size();
	}

	public void addImgLink(String imgLink) {
		this.imgLinks.add(imgLink);
		TPreConsumer.getInstance().getThread().notify();
	}

	
	
	
}
